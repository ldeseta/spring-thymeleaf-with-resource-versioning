package com.dosideas.demoresourceversioning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoResourceVersioningApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoResourceVersioningApplication.class, args);
    }
}
